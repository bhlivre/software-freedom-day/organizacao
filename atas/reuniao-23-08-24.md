### Ata da reunião do dia 24/08/2023

A reunião foi online e estavam presentes: Bruno, Fred, Hérico e Wanderlei.

- Iniciamos a reunião com o Fred esplicando e esclarecendo algumas dúvidas sobre a natureza do evento e como se organizam eventos desse tipo. Destacando que, apesar de ser no CEFET, o evento não tem um caráter acadêmico e o público é bastante diverso.
- Wanderlei demonstrou interesse em apresentar sobre o software livre e o licenciamento. Gostaria de montar a apresentação e repassar com os integrantes do grupo sobre o conteúdo.
- Hérico sugeriu fazer uma apresentação mostrando como montar um servidor de e-mail utilizando softwares livres, apresentando um estudo de caso de personalização do K9-Mail.
- Fred sugeriu que nos concentremos na divulgação do evento até o dia 30 de agosto, quando se encerram as chamadas de trabalho, com a proposta de divulgar em mídias sociais ou presencialmente. O endereço de referência para o evento é: <http://sfd.softwarelivre.bhz.br/>.
- Hérico ressaltou a importância de se usar o e-mail como forma de divulgação. Inclusive existem listas de e-mail na UFMG que podem ser usadas para isso. Bruno e Hérico irão verificar a possibilidade de fazer a divulgação nessas listas.
- Wanderlei irá entrar em contato com o China para ver possibilidades de divugalção no SERPRO.
- Hérico sugeriu que o texto da chamada de trabalhos seja mais explicativa, discriminando melhor as opções de temas. Além disso, Bruno sugeriu que essa explicação conste também no próprio formulário.
- Ficou uma dúvida ao final se vamos ter ou não workshops no evento. Iremos discutir isso no grupo.
